#[cfg(test)]
mod tests {
    use crate::{Interval, Map, MapLine};

    #[test]
    fn interval_difference_can_return_nothing() {
        let a = Interval::from_bounds(1, 3).unwrap();
        let b = Interval::from_bounds(0, 5).unwrap();

        assert_eq!(a.difference(&b), vec![]);
    }

    #[test]
    fn interval_difference_can_return_one_interval() {
        let a = Interval::from_bounds(1, 3).unwrap();
        let b = Interval::from_bounds(2, 5).unwrap();
        let r = Interval::from_bounds(1, 1).unwrap();

        assert_eq!(a.difference(&b), vec![r]);
    }

    #[test]
    fn interval_difference_can_return_two_intervals() {
        let a = Interval::from_bounds(1, 10).unwrap();
        let b = Interval::from_bounds(4, 5).unwrap();
        let r = vec![
            Interval::from_bounds(1, 3).unwrap(),
            Interval::from_bounds(6, 10).unwrap(),
        ];


        assert_eq!(a.difference(&b), r);
    }

    #[test]
    fn map_is_idempotent_without_rules() {
        let m = Map {
            ranges: vec![]
        };
        let i = Interval::from_bounds(1, 10).unwrap();
        assert_eq!(m.transform_intervals(vec![i.clone()]), vec![i.clone()]);
    }

    #[test]
    fn map_can_move_a_whole_interval() {
        let m = Map {
            ranges: vec![MapLine{
                source_start: 1,
                destination_start: 6,
                length: 5,
            }]
        };
        let i = Interval::from_bounds(1, 3).unwrap();
        assert_eq!(m.transform_intervals(vec![i]), vec![Interval::from_bounds(6, 8).unwrap()]);
    }
}
