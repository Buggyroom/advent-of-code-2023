mod test;

use std::{
    cmp::{max, min},
    fs::File,
    io::{BufReader, Read},
};

use nom::{
    bytes::complete::tag,
    character::complete::{alpha1, multispace0, space1},
    multi::separated_list1,
    sequence::{delimited, preceded, separated_pair, terminated},
    IResult,
};

fn main() {
    let args: Vec<String> = std::env::args().collect();
    if args.len() != 2 {
        println!("usage : {} FILE", &args[0]);
        std::process::exit(1);
    }
    let f = File::open(&args[1]).unwrap();
    let mut reader = BufReader::new(f);
    let mut buffer = String::new();
    let _ = reader.read_to_string(&mut buffer).unwrap();
    let (_, almanac) = parse_almanac(buffer.as_str()).unwrap();
    let intervals = almanac.transform_intervals(almanac.seeds.clone());
    let result = intervals.iter().map(|i| i.start).min().unwrap();
    println!("Result : {}", result);
}

struct Almanac {
    seeds: Vec<Interval>,
    maps: Vec<Map>,
}

impl Almanac {
    fn transform_intervals(&self, input: Vec<Interval>) -> Vec<Interval> {
        let mut intervals: Vec<Interval> = input;
        for map in &self.maps {
            intervals = map.transform_intervals(intervals);
        }
        return intervals;
    }
}

fn parse_almanac(input: &str) -> IResult<&str, Almanac> {
    let (remainder, val) = delimited(
        multispace0,
        separated_pair(
            parse_seeds,
            multispace0,
            separated_list1(multispace0, parse_map),
        ),
        multispace0,
    )(input)?;

    return Ok((
        remainder,
        Almanac {
            seeds: val.0,
            maps: val.1,
        },
    ));
}

fn parse_seeds(input: &str) -> IResult<&str, Vec<Interval>> {
    return preceded(tag("seeds: "), separated_list1(space1, parse_seed_range))(input);
}

fn parse_seed_range(input: &str) -> IResult<&str, Interval> {
    let (remainder, val) = separated_pair(
        nom::character::complete::i64,
        space1,
        nom::character::complete::i64,
    )(input)?;
    return Ok((
        remainder,
        Interval {
            start: val.0,
            size: val.1,
        },
    ));
}

struct Map {
    ranges: Vec<MapLine>,
}

impl Map {
    fn transform_intervals(&self, input: Vec<Interval>) -> Vec<Interval> {
        let mut input_intervals: Vec<Interval> = input;
        let mut mapped_intervals: Vec<Interval> = Vec::new();
        for range in &self.ranges {
            let range_interval = Interval {
                start: range.source_start,
                size: range.length,
            };
            let mut new_input_intervals: Vec<Interval> = Vec::new();
            for interval in input_intervals {
                if let Some(i) = interval.intersection(&range_interval) {
                    mapped_intervals.push(Interval {
                        start: i.start + range.destination_start - range.source_start,
                        size: i.size,
                    });
                }
                new_input_intervals.extend(interval.difference(&range_interval));
            }
            input_intervals = new_input_intervals;
        }
        mapped_intervals.extend(input_intervals);
        return mapped_intervals;
    }
}

#[derive(Clone, PartialEq, Eq, Debug)]
struct Interval {
    start: i64,
    size: i64,
}

impl Interval {
    fn end(&self) -> i64 {
        return self.start + self.size - 1;
    }

    fn from_bounds(start: i64, end: i64) -> Option<Self> {
        if start <= end {
            return Some(Interval {
                start,
                size: end - start + 1,
            });
        } else {
            return None;
        }
    }

    fn intersection(&self, other: &Self) -> Option<Interval> {
        let start = max(self.start, other.start);
        let end = min(self.end(), other.end());
        return Interval::from_bounds(start, end);
    }

    fn difference(&self, other: &Self) -> Vec<Interval> {
        /* compute as self ⋂ other'

           other  :       [----]
           other' : <----]      [----->
           self   :    [----------]
        */
        let mut res: Vec<Interval> = Vec::new();
        /* check for left part

                       vvvv
           other' : <-----]
           self   :    [----------]
        */
        {
            let end = min(self.end(), other.start - 1);
            if let Some(i) = Interval::from_bounds(self.start, end) {
                res.push(i);
            }
        }
        /* check for right part

                               vvvv
           other' :            [------>
           self   :    [----------]
        */
        {
            let start = max(self.start, other.end() + 1);
            if let Some(i) = Interval::from_bounds(start, self.end()) {
                res.push(i);
            }
        }
        return res;
    }
}

fn parse_map(input: &str) -> IResult<&str, Map> {
    let (remainder, value) = separated_pair(
        parse_map_kind,
        multispace0,
        separated_list1(multispace0, parse_map_line),
    )(input)?;
    return Ok((
        remainder,
        Map {
            ranges: value.1,
        },
    ));
}

fn parse_map_kind(input: &str) -> IResult<&str, ()> {
    let (remainder, _) =
        terminated(separated_pair(alpha1, tag("-to-"), alpha1), tag(" map:"))(input)?;

    return Ok((
        remainder,
        (),
    ));
}

struct MapLine {
    source_start: i64,
    destination_start: i64,
    length: i64,
}

fn parse_map_line(input: &str) -> IResult<&str, MapLine> {
    let (remainder, val) = separated_list1(space1, nom::character::complete::i64)(input)?;

    if val.len() != 3 {
        panic!("expected 3 numbers, found {}", val.len());
    }
    return Ok((
        remainder,
        MapLine {
            source_start: val[1],
            destination_start: val[0],
            length: val[2],
        },
    ));
}
