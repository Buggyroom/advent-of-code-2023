use std::{
    fs::File,
    io::{BufReader, Read},
};

use nom::{
    bytes::complete::tag,
    character::complete::{multispace1, space1},
    multi::separated_list1,
    sequence::{preceded, separated_pair, tuple},
    IResult,
};

fn main() {
    let args: Vec<String> = std::env::args().collect();
    if args.len() != 2 {
        println!("usage: {} FILE", &args[0]);
    }
    let f = File::open(&args[1]).unwrap();
    let mut reader = BufReader::new(f);
    let mut buffer = String::new();
    let _ = reader.read_to_string(&mut buffer).unwrap();
    let (_, races) = parse_races(buffer.as_str()).unwrap();
    let result: u32 = races
        .iter()
        .map(|r| r.naive_number_of_ways_to_beat_record())
        .product();
    println!("Result: {}", result);
}

struct Race {
    time: u32,
    record_distance: u32,
}

impl Race {
    fn naive_number_of_ways_to_beat_record(&self) -> u32 {
        return (1..self.time)
            .map(|t| self.distance_if_pushed_for(t))
            .filter(|d| d > &self.record_distance)
            .count() as u32;
    }

    fn distance_if_pushed_for(&self, duration: u32) -> u32 {
        let speed = duration;
        let time_in_motion = self.time.saturating_sub(duration);
        return speed * time_in_motion;
    }
}

fn parse_races(input: &str) -> IResult<&str, Vec<Race>> {
    let (remainder, (times, distances)) =
        separated_pair(parse_time, multispace1, parse_distance)(input)?;

    assert_eq!(times.len(), distances.len());
    return Ok((
        remainder,
        times
            .iter()
            .zip(distances.iter())
            .map(|(t, d)| Race {
                time: *t,
                record_distance: *d,
            })
            .collect(),
    ));
}

fn parse_time(input: &str) -> IResult<&str, Vec<u32>> {
    return preceded(
        tuple((tag("Time:"), space1)),
        separated_list1(space1, nom::character::complete::u32),
    )(input);
}

fn parse_distance(input: &str) -> IResult<&str, Vec<u32>> {
    return preceded(
        tuple((tag("Distance:"), space1)),
        separated_list1(space1, nom::character::complete::u32),
    )(input);
}
