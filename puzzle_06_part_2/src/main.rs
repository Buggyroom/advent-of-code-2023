use std::{
    fs::File,
    io::{BufReader, Read},
};

use nom::{
    bytes::complete::tag,
    character::complete::{digit1, multispace1, space1},
    multi::separated_list1,
    sequence::{preceded, separated_pair, tuple},
    IResult,
};

fn main() {
    let args: Vec<String> = std::env::args().collect();
    if args.len() != 2 {
        println!("usage: {} FILE", &args[0]);
    }
    let f = File::open(&args[1]).unwrap();
    let mut reader = BufReader::new(f);
    let mut buffer = String::new();
    let _ = reader.read_to_string(&mut buffer).unwrap();
    let (_, race) = parse_race(buffer.as_str()).unwrap();
    let result = race.smart_number_of_ways_to_beat_record();
    println!("Result: {}", result);
}

struct Race {
    time: u64,
    record_distance: u64,
}

impl Race {
    fn smart_number_of_ways_to_beat_record(&self) -> f64 {
        /*
        if h is the button hold duration, the boat's speed is equal to h by
        definition. if D is the race duration, the boat will move for D - h ms
        which means it will travel h * (D - h) mm

        Finding the hold duration that beat the record is the same as solving
        this inequality :
            
            h * (D - h) > R 
        
        which in turn requires to solve this 2nd degree polynomial :

            -h² + Dh - R = 0
        */
        let a: f64 = -1.0;
        let b: f64 = self.time as f64;
        let c: f64 = -(self.record_distance as f64);

        let discriminant = b.powi(2) - 4.0 * a * c;
        let lower_root = (-b + discriminant.sqrt()) / (2.0 * a);
        let upper_root = (-b - discriminant.sqrt()) / (2.0 * a);
        let lower_time = lower_root.ceil();
        let upper_time = upper_root.floor();
        return upper_time - lower_time + 1.0;
    }
}

fn parse_race(input: &str) -> IResult<&str, Race> {
    let (remainder, (time, record_distance)) =
        separated_pair(parse_time, multispace1, parse_distance)(input)?;
    return Ok((
        remainder,
        Race {
            time,
            record_distance,
        },
    ));
}

fn parse_time(input: &str) -> IResult<&str, u64> {
    let (remainder, val) = preceded(
        tuple((tag("Time:"), space1)),
        separated_list1(space1, digit1),
    )(input)?;

    let mut num = String::new();
    val.iter().for_each(|s| num.push_str(s));
    return Ok((remainder, num.parse::<u64>().unwrap()));
}

fn parse_distance(input: &str) -> IResult<&str, u64> {
    let (remainder, val) = preceded(
        tuple((tag("Distance:"), space1)),
        separated_list1(space1, digit1),
    )(input)?;

    let mut num = String::new();
    val.iter().for_each(|s| num.push_str(s));
    return Ok((remainder, num.parse::<u64>().unwrap()));
}
