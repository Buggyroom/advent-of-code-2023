use std::{
    fs::File,
    io::{BufReader, Read}, collections::HashMap,
};

use nom::{
    branch::alt,
    bytes::complete::tag,
    character::complete::{one_of, multispace1},
    combinator::{recognize, value},
    multi::{count, many1, separated_list1},
    sequence::{delimited, separated_pair, tuple},
    IResult,
};

fn main() {
    let args: Vec<String> = std::env::args().collect();
    if args.len() != 2 {
        println!("usage: {} FILE", &args[0]);
        std::process::exit(1);
    }
    let f = File::open(&args[1]).unwrap();
    let mut reader = BufReader::new(f);
    let mut buffer: String = String::new();
    let _ = reader.read_to_string(&mut buffer).unwrap();
    let (_, map) = parse_input(buffer.as_str()).unwrap();
    let result = map.count_steps().unwrap();
    println!("Result : {}", result);
}

struct Map<'a> {
    directions: Vec<bool>,
    neighbors: HashMap<&'a str, (&'a str, &'a str)>
}

impl<'a> Map<'a> {
    fn count_steps(&self) -> Option<usize> {
        let mut current_node = "AAA";
        for (i, &turn_left) in self.directions.iter().cycle().enumerate() {
            let (left, right) = self.neighbors.get(current_node).unwrap();
            current_node = if turn_left {left} else {right};
            if current_node == "ZZZ" {
                return Some(i + 1);
            }
        }
        return None;
    }
}

fn parse_input(input: &str) -> IResult<&str, Map> {
    let (remainder, (directions, nodes)) = separated_pair(
        parse_directions,
        multispace1,
        separated_list1(multispace1, parse_node)
    )(input)?;

    let mut neighbors: HashMap<&str, (&str, &str)> = HashMap::new();
    for Node { name, left, right } in nodes {
        neighbors.insert(name, (left, right));
    }
    return Ok((remainder, Map {directions, neighbors}));
}

fn parse_directions(input: &str) -> IResult<&str, Vec<bool>> {
    return many1(alt((
        value(true, tag("L")),
        value(false, tag("R"))
    )))(input);
}

struct Node<'a> {
    name: &'a str,
    left: &'a str,
    right: &'a str,
}

fn parse_node(input: &str) -> IResult<&str, Node> {
    let (remainder, val) = tuple((
        parse_node_name,
        tag(" = "),
        delimited(
            tag("("),
            separated_pair(parse_node_name, tag(", "), parse_node_name),
            tag(")"),
        ),
    ))(input)?;

    return Ok((remainder, Node {name: val.0, left: val.2.0, right: val.2.1}));
}

fn parse_node_name(input: &str) -> IResult<&str, &str> {
    return recognize(count(one_of("ABCDEFGHIJKLMNOPQRSTUVWXYZ"), 3))(input);
}
