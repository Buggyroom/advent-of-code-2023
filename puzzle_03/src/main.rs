use std::collections::HashSet;
use std::io::{BufReader, BufRead};

use nom::IResult;
use nom::branch::alt;
use nom::bytes::complete::{take, tag};
use nom::combinator::{value, map, peek, recognize, all_consuming};
use nom::multi::many1;
use nom::sequence::tuple;


fn main() {
    let args: Vec<String> = std::env::args().collect();
    if args.len() != 2 {
        println!("usage: {} FILE", &args[0]);
        std::process::exit(1);
    }
    let f = std::fs::File::open(&args[1]).unwrap();
    let reader = BufReader::new(f);
    let mut numbers: Vec<Number> = Vec::new();
    let mut symbols: HashSet<Position> = HashSet::new();
    for (y, line) in reader.lines().enumerate() {
        let line = line.unwrap();
        let (_, parsed_line) = parse_line(&line).unwrap();
        for n in parsed_line.numbers {
            numbers.push(Number{
                value: n.value,
                length: n.length as u32,
                position: Position { x: n.position as u32, y: y as u32 }
            });
        }
        for s in parsed_line.symbols {
            symbols.insert(Position { x: s.position as u32, y: y as u32 });
        }
    }
    let mut total: u32 = 0;
    for number in numbers {
        let Number {position: Position {x, y}, value, length} = number;
        // top left
        if x >= 1 && y >= 1 {
            if symbols.contains(&Position { x: x-1, y: y-1 }) {
                total += value;
                continue;
            }
        }
        // top
        if y >= 1 {
            for i in 0..length {
                if symbols.contains(&Position { x: x+i, y: y-1 }) {
                    total += value;
                    continue;
                }
            }
        }
        // top right
        if y >= 1 {
            if symbols.contains(&Position { x: x + length, y: y-1 }) {
                total += value;
                continue;
            }
        }
        // left
        if x >= 1 {
            if symbols.contains(&Position { x: x - 1, y: y }) {
                total += value;
                continue;
            }
        }
        // right
        if symbols.contains(&Position { x: x + length, y: y }) {
            total += value;
            continue;
        }
        // bottom left
        if x >= 1 {
            if symbols.contains(&Position { x: x - 1, y: y + 1 }) {
                total += value;
                continue;
            }
        }
        // bottom
        for i in 0..length {
            if symbols.contains(&Position { x: x + i, y: y + 1 }) {
                total += value;
                continue;
            }
        }
        // bottom right
        if symbols.contains(&Position { x: x + length, y: y + 1 }) {
            total += value;
            continue;
        }
    }
    println!("Total : {}", total);
}

#[derive(Debug)]
struct Number {
    value: u32,
    length: u32,
    position: Position
}

#[derive(Debug, PartialEq, Eq, Hash)]
struct Position {
    x: u32,
    y: u32,
}

struct NumberInLine {
    value: u32,
    length: usize,
    position: usize
}

struct SymbolInLine {
    position: usize
}

struct Line {
    numbers: Vec<NumberInLine>,
    symbols: Vec<SymbolInLine>,
}

fn parse_line(input: &str) -> IResult<&str, Line> {
    let (remainder, atoms) = all_consuming(many1(parse_atom))(input)?;
    let mut line = Line {
        numbers: Vec::new(),
        symbols: Vec::new(),
    };
    let mut i: usize = 0;
    for atom in atoms {
        match atom {
            Atom::Dot => i += 1,
            Atom::Symbol => {
                line.symbols.push(SymbolInLine { position: i });
                i += 1;
            }
            Atom::Number{value, length} => {
                line.numbers.push(NumberInLine { value: value, length: length, position: i });
                i += length;
            }
        }
    }
    return Ok((remainder, line));
}

#[derive(Clone, Copy)]
enum Atom {
    Dot,
    Symbol,
    Number {
        value: u32,
        length: usize
    }
}

fn parse_atom(input: &str) -> IResult<&str, Atom> {
    return alt((
        map(
            tuple((
                peek(nom::character::complete::u32),
                recognize(nom::character::complete::u32),
            )),
            |t: (u32, &str)| Atom::Number{
                value: t.0,
                length: t.1.len(),
            }
        ),
        value(Atom::Dot, tag(".")),
        value(Atom::Symbol, take(1usize)),
    ))(input);
}
