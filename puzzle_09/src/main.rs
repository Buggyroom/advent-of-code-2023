use std::io::{BufRead, BufReader};
use std::fs::File;

use nom::IResult;
use nom::character::complete::space1;
use nom::multi::separated_list1;

fn main() {
    let args: Vec<String> = std::env::args().collect();
    if args.len() != 2 {
        println!("usage: {} FILE", &args[0]);
        std::process::exit(1);
    }
    let f = File::open(&args[1]).unwrap();
    let reader = BufReader::new(f);
    let results: i64 = reader
        .lines()
        .map(|l| l.unwrap())
        .map(|str| parse_sequence(str.as_str()).unwrap().1)
        .map(|seq| extrapolate(&seq))
        .sum();
    println!("Results : {}", results);
}

fn extrapolate(sequence: &Vec<i64>) -> i64 {
    let mut derivates: Vec<Vec<i64>> = Vec::new();
    derivates.push(differenciate(&sequence));
    while derivates.last().unwrap().iter().any(|x| *x != 0) {
        derivates.push(differenciate(&derivates.last().unwrap()));
    }
    return derivates.iter().rev().fold(0i64, |acc, x| acc + x.last().unwrap()) + sequence.last().unwrap();
}

fn differenciate(sequence: &Vec<i64>) -> Vec<i64> {
    return sequence.iter().skip(1).zip(sequence.iter()).map(|(a, b)| a - b).collect();
}

fn parse_sequence(input: &str) -> IResult<&str, Vec<i64>> {
    return separated_list1(space1, nom::character::complete::i64)(input);
}
