use std::fs::File;
use std::io::{BufRead, BufReader};

fn main() {
    let args: Vec<String> = std::env::args().collect();
    if args.len() != 2 {
        println!("usage : {} FILE", &args[0]);
        std::process::exit(1);
    }
    let f = File::open(&args[1]).unwrap();
    let reader = BufReader::new(f);
    let mut map: Vec<Vec<char>> = Vec::new();
    let mut starting_position: Option<Position> = None;
    for (y, line) in reader.lines().enumerate() {
        let line = line.unwrap();
        map.push(line.chars().collect());
        if let Some(x) = line.find("S") {
            starting_position = Some(Position{x, y});
        }
    }
    let start = starting_position.unwrap();
    let mut prev = starting_position.unwrap();
    let mut pos = Position{x: prev.x, y: prev.y - 1};
    let mut loop_size: usize = 1;
    while pos != start {
        let tile = map[pos.y][pos.x];
        let (a, b) = match tile {
            '|' => (Position{x: pos.x, y: pos.y - 1}, Position{x: pos.x, y: pos.y + 1}),
            '-' => (Position{x: pos.x - 1, y: pos.y}, Position{x: pos.x + 1, y: pos.y}),
            'L' => (Position{x: pos.x + 1, y: pos.y}, Position{x: pos.x, y: pos.y - 1}),
            'J' => (Position{x: pos.x - 1, y: pos.y}, Position{x: pos.x, y: pos.y - 1}),
            '7' => (Position{x: pos.x - 1, y: pos.y}, Position{x: pos.x, y: pos.y + 1}),
            'F' => (Position{x: pos.x + 1, y: pos.y}, Position{x: pos.x, y: pos.y + 1}),
            t => panic!("Unexpected tile : {}", t),
        };
        match (a == prev, b == prev) {
            (true, true) => panic!("both ends equal to previous position"),
            (false, false) => panic!("neither ends are previous position"),
            (true, false) => {
                prev = pos;
                pos = b;
            }
            (false, true) => {
                prev = pos;
                pos = a;
            }
        }
        loop_size += 1;
    }
    let result = loop_size / 2;
    println!("Result : {}", result);
}

#[derive(Clone, Copy, PartialEq, Eq)]
struct Position {
    x: usize,
    y: usize
}
