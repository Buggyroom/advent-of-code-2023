use std::{
    collections::HashMap,
    fs::File,
    io::{BufReader, Read},
};

use nom::{
    branch::alt,
    bytes::complete::tag,
    character::complete::{multispace1, one_of},
    combinator::{recognize, value},
    multi::{count, many1, separated_list1},
    sequence::{delimited, separated_pair, tuple},
    IResult,
};

fn main() {
    let args: Vec<String> = std::env::args().collect();
    if args.len() != 2 {
        println!("usage: {} FILE", &args[0]);
        std::process::exit(1);
    }
    let f = File::open(&args[1]).unwrap();
    let mut reader = BufReader::new(f);
    let mut buffer: String = String::new();
    let _ = reader.read_to_string(&mut buffer).unwrap();
    let (_, map) = parse_input(buffer.as_str()).unwrap();
    let starting_nodes = map.find_starting_nodes();
    let loop_and_end_points: Vec<_> = starting_nodes
        .iter()
        .map(|n| map.find_loop_and_end_points(n))
        .collect();
    for (_loop, end_points) in &loop_and_end_points {
        let _loop = _loop.as_ref().unwrap();
        assert_eq!(end_points.len(), 1);
        assert_eq!(_loop.size, end_points[0].position);
    }
    let result = loop_and_end_points
        .iter()
        .map(|(l, _)| l.as_ref().unwrap().size)
        .reduce(|acc, e| num::integer::lcm(acc, e)).unwrap();
    println!("Result : {}", result);
}

struct Map<'a> {
    directions: Vec<bool>,
    neighbors: HashMap<&'a str, (&'a str, &'a str)>,
}

#[derive(Debug)]
struct Loop {
    size: usize,
}

#[derive(Debug)]
struct EndPoint {
    position: usize,
}

impl<'a> Map<'a> {
    fn find_loop_and_end_points(
        &self,
        starting_point: &'a str,
    ) -> (Option<Loop>, Vec<EndPoint>) {
        let mut node = starting_point;
        let mut _loop = None;
        let mut end_points: Vec<EndPoint> = Vec::new();
        let mut previous_positions: HashMap<(&'a str, usize), usize> = HashMap::new();
        for (total_position, (gps_index, &turn_left)) in
            self.directions.iter().enumerate().cycle().enumerate()
        {
            if node.chars().last() == Some('Z') {
                end_points.push(EndPoint {
                    position: total_position,
                });
            }
            let prev_pos = (node, gps_index);
            if let Some(pos) = previous_positions.get(&prev_pos) {
                _loop = Some(Loop {
                    size: total_position - pos,
                });
                break;
            } else {
                previous_positions.insert(prev_pos, total_position);
            }
            let (left, right) = self.neighbors.get(node).unwrap();
            node = if turn_left { left } else { right };
        }
        return (_loop, end_points);
    }

    fn find_starting_nodes(&self) -> Vec<&'a str> {
        return self
            .neighbors
            .keys()
            .filter(|k| k.chars().last() == Some('A'))
            .map(|k| *k)
            .collect();
    }
}

fn parse_input(input: &str) -> IResult<&str, Map> {
    let (remainder, (directions, nodes)) = separated_pair(
        parse_directions,
        multispace1,
        separated_list1(multispace1, parse_node),
    )(input)?;

    let mut neighbors: HashMap<&str, (&str, &str)> = HashMap::new();
    for Node { name, left, right } in nodes {
        neighbors.insert(name, (left, right));
    }
    return Ok((
        remainder,
        Map {
            directions,
            neighbors,
        },
    ));
}

fn parse_directions(input: &str) -> IResult<&str, Vec<bool>> {
    return many1(alt((value(true, tag("L")), value(false, tag("R")))))(input);
}

struct Node<'a> {
    name: &'a str,
    left: &'a str,
    right: &'a str,
}

fn parse_node(input: &str) -> IResult<&str, Node> {
    let (remainder, val) = tuple((
        parse_node_name,
        tag(" = "),
        delimited(
            tag("("),
            separated_pair(parse_node_name, tag(", "), parse_node_name),
            tag(")"),
        ),
    ))(input)?;

    return Ok((
        remainder,
        Node {
            name: val.0,
            left: val.2 .0,
            right: val.2 .1,
        },
    ));
}

fn parse_node_name(input: &str) -> IResult<&str, &str> {
    return recognize(count(one_of("ABCDEFGHIJKLMNOPQRSTUVWXYZ"), 3))(input);
}
