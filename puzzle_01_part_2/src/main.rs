use std::env;
use std::fs::File;
use std::io::{BufReader, BufRead};
use std::process::exit;

fn main() {
    let args: Vec<String> = env::args().collect();
    if args.len() != 2 {
        println!("usage : {} FILE", args[0]);
        exit(1);
    }
    let f = File::open(&args[1]).unwrap();
    let reader = BufReader::new(f);
    let mut count: u32 = 0;
    for (index, line) in reader.lines().enumerate() {
        let line = line.unwrap();
        let chars: Vec<char> = line.chars().collect();
        let mut digits: Vec<u32> = Vec::new();
        for i in 0..chars.len() {
            let char = chars[i];
            if char.is_ascii_digit() {
                digits.push(char.to_digit(10).unwrap());
                continue;
            }
            match &chars[i..] {
                ['o', 'n', 'e', ..] => {
                    digits.push(1);
                }
                ['t', 'w', 'o', ..] => {
                    digits.push(2);
                }
                ['t', 'h', 'r', 'e', 'e', ..] => {
                    digits.push(3);
                }
                ['f', 'o', 'u', 'r', ..] => {
                    digits.push(4);
                }
                ['f', 'i', 'v', 'e', ..] => {
                    digits.push(5);
                }
                ['s', 'i', 'x', ..] => {
                    digits.push(6);
                }
                ['s', 'e', 'v', 'e', 'n', ..] => {
                    digits.push(7);
                }
                ['e', 'i', 'g', 'h', 't', ..] => {
                    digits.push(8);
                }
                ['n', 'i', 'n', 'e', ..] => {
                    digits.push(9);
                }
                _ => {}
            }
        }
        if digits.len() < 1 {
            println!("Line {index} has not digits ... aborting");
            exit(1);
        }
        let first_digit = digits.first().unwrap();
        let last_digit = digits.last().unwrap();
        count += first_digit * 10 + last_digit;
    }
    println!("Total : {count}");
}
