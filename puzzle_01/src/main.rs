use std::env;
use std::fs::File;
use std::io::{BufReader, BufRead};
use std::process::exit;

fn main() {
    let args: Vec<String> = env::args().collect();
    if args.len() != 2 {
        println!("usage : {} FILE", args[0]);
        exit(1);
    }
    let f = File::open(&args[1]).unwrap();
    let reader = BufReader::new(f);
    let mut count: u32 = 0;
    for (i, line) in reader.lines().enumerate() {
        let line = line.unwrap();
        let digits: Vec<char> = line.chars().filter(|c| c.is_ascii_digit()).collect();
        if digits.len() < 1 {
            println!("Line {i} has not digits ... aborting");
            exit(1);
        }
        let first_digit: u32 = digits.first().unwrap().to_digit(10).unwrap();
        let last_digit: u32 = digits.last().unwrap().to_digit(10).unwrap();
        count += first_digit * 10 + last_digit;
    }
    println!("Total : {count}");
}
