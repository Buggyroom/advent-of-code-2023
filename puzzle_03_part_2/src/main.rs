use std::collections::{HashSet, HashMap};
use std::io::{BufReader, BufRead};

use nom::IResult;
use nom::branch::alt;
use nom::bytes::complete::{take, tag};
use nom::combinator::{value, map, peek, recognize, all_consuming};
use nom::multi::many1;
use nom::sequence::tuple;


fn main() {
    let args: Vec<String> = std::env::args().collect();
    if args.len() != 2 {
        println!("usage: {} FILE", &args[0]);
        std::process::exit(1);
    }
    let f = std::fs::File::open(&args[1]).unwrap();
    let reader = BufReader::new(f);
    let mut numbers: Vec<Number> = Vec::new();
    let mut gears: HashSet<Position> = HashSet::new();
    for (y, line) in reader.lines().enumerate() {
        let line = line.unwrap();
        let (_, parsed_line) = parse_line(&line).unwrap();
        for n in parsed_line.numbers {
            numbers.push(Number{
                value: n.value,
                length: n.length as u32,
                position: Position { x: n.position as u32, y: y as u32 }
            });
        }
        for g in parsed_line.gears {
            gears.insert(Position { x: g.position as u32, y: y as u32 });
        }
    }
    let mut numbers_touched_by_gear: HashMap<Position, Vec<Number>> = HashMap::new();
    for number in numbers {
        let Number {position: Position {x, y}, length, ..} = number;
        // top left
        if x >= 1 && y >= 1 {
            let pos = Position { x: x-1, y: y-1 };
            if gears.contains(&pos) {
                numbers_touched_by_gear.entry(pos).or_insert(Vec::new()).push(number.clone());
            }
        }
        // top
        if y >= 1 {
            for i in 0..length {
                let pos = Position { x: x+i, y: y-1 };
                if gears.contains(&pos) {
                    numbers_touched_by_gear.entry(pos).or_insert(Vec::new()).push(number.clone());
                }
            }
        }
        // top right
        if y >= 1 {
            let pos = Position { x: x + length, y: y-1 };
            if gears.contains(&pos) {
                numbers_touched_by_gear.entry(pos).or_insert(Vec::new()).push(number.clone());
            }
        }
        // left
        if x >= 1 {
            let pos = Position { x: x - 1, y: y };
            if gears.contains(&pos) {
                numbers_touched_by_gear.entry(pos).or_insert(Vec::new()).push(number.clone());
            }
        }
        // right
        let pos = Position { x: x + length, y: y };
        if gears.contains(&pos) {
            numbers_touched_by_gear.entry(pos).or_insert(Vec::new()).push(number.clone());
        }
        // bottom left
        if x >= 1 {
            let pos = Position { x: x - 1, y: y + 1 };
            if gears.contains(&pos) {
                numbers_touched_by_gear.entry(pos).or_insert(Vec::new()).push(number.clone());
            }
        }
        // bottom
        for i in 0..length {
            let pos = Position { x: x + i, y: y + 1 };
            if gears.contains(&pos) {
                numbers_touched_by_gear.entry(pos).or_insert(Vec::new()).push(number.clone());
            }
        }
        // bottom right
        let pos = Position { x: x + length, y: y + 1 };
        if gears.contains(&pos) {
            numbers_touched_by_gear.entry(pos).or_insert(Vec::new()).push(number.clone());
        }
    }
    let total: u32 = numbers_touched_by_gear
            .iter()
            .filter(|(_, v)| v.len() == 2)
            .map(|(_, v)| v[0].value * v[1].value)
            .sum();
    println!("Total : {}", total);
}

#[derive(Debug, Clone)]
struct Number {
    value: u32,
    length: u32,
    position: Position
}

#[derive(Debug, PartialEq, Eq, Hash, Clone)]
struct Position {
    x: u32,
    y: u32,
}

struct NumberInLine {
    value: u32,
    length: usize,
    position: usize
}

struct GearInLine {
    position: usize
}

struct Line {
    numbers: Vec<NumberInLine>,
    gears: Vec<GearInLine>,
}

fn parse_line(input: &str) -> IResult<&str, Line> {
    let (remainder, atoms) = all_consuming(many1(parse_atom))(input)?;
    let mut line = Line {
        numbers: Vec::new(),
        gears: Vec::new(),
    };
    let mut i: usize = 0;
    for atom in atoms {
        match atom {
            Atom::Dot | Atom::Symbol => i += 1,
            Atom::Gear => {
                line.gears.push(GearInLine { position : i });
                i += 1;
            }
            Atom::Number{value, length} => {
                line.numbers.push(NumberInLine { value: value, length: length, position: i });
                i += length;
            }
        }
    }
    return Ok((remainder, line));
}

#[derive(Clone, Copy)]
enum Atom {
    Dot,
    Gear,
    Symbol,
    Number {
        value: u32,
        length: usize
    }
}

fn parse_atom(input: &str) -> IResult<&str, Atom> {
    return alt((
        map(
            tuple((
                peek(nom::character::complete::u32),
                recognize(nom::character::complete::u32),
            )),
            |t: (u32, &str)| Atom::Number{
                value: t.0,
                length: t.1.len(),
            }
        ),
        value(Atom::Dot, tag(".")),
        value(Atom::Gear, tag("*")),
        value(Atom::Symbol, take(1usize)),
    ))(input);
}