use std::{fs::File, io::{BufRead, BufReader}};

use nom::{IResult, branch::alt, combinator::value, bytes::complete::tag, sequence::separated_pair, multi::{many1, separated_list1}, character::complete::space1};

fn main() {
    let args: Vec<String> = std::env::args().collect();
    if args.len() != 2 {
        println!("usage: {} FILE", &args[0]);
    }
    let f = File::open(&args[1]).unwrap();
    let reader = BufReader::new(f);
    let result: usize = reader
        .lines()
        .map(|l| l.unwrap())
        .map(|l| parse_puzzle(&l).unwrap().1)
        .map(|p| p.count_possible_solutions())
        .sum();
    println!("Result: {}", result);
}

#[derive(PartialEq, Eq, Debug)]
struct Puzzle {
    tiles: Vec<Tile>,
    continuous_damaged: Vec<u8>
}

impl Puzzle {
    fn count_possible_solutions(&self) -> usize {
        let root = SolutionCandidate::from_puzzle(self);
        return root.backtrack(&self.continuous_damaged);
    }
}

#[derive(Clone, PartialEq, Eq, Debug)]
struct SolutionCandidate {
    tiles: Vec<Tile>,
    unknown_tiles: u8,
    damaged_tiles_to_place: u8,
}

fn count_continous_damaged(tiles: &Vec<Tile>) -> Vec<u8> {
    let mut res = Vec::<u8>::new();
    let mut length: u8 = 0;
    for tile in tiles {
        match tile {
            Tile::Unknown | Tile::Operational => {
                if length != 0 {
                    res.push(length);
                    length = 0;
                }
            }
            Tile::Damaged => {
                length += 1;
            }
        }
    }
    if length != 0 {
        res.push(length);
    }
    return res;
}

impl SolutionCandidate {
    fn from_puzzle(puzzle: &Puzzle) -> Self {
        let total_damaged: u8 = puzzle.continuous_damaged.iter().sum();
        let current_damaged: u8 = puzzle.tiles.iter().filter(|t| matches!(t, Tile::Damaged)).count() as u8;
        return Self {
            tiles: puzzle.tiles.clone(),
            unknown_tiles: puzzle.tiles.iter().filter(|t| matches!(t, Tile::Unknown)).count() as u8,
            damaged_tiles_to_place: total_damaged.saturating_sub(current_damaged)
        }
    }

    fn backtrack(&self, target_continous_damaged: &Vec<u8>) -> usize {
        let mut count = 0;
        if self.reject(target_continous_damaged) {
            return count;
        } else if self.accept(target_continous_damaged) {
            return count + 1;
        }
        let children = ChildSolutions::from_candidate(self);
        for child in children {
            count += child.backtrack(target_continous_damaged);
        }
        return count;
    }

    fn reject(&self, target_continous_damaged: &Vec<u8>) -> bool {
        if self.unknown_tiles < self.damaged_tiles_to_place {
            return true;
        }
        if self.damaged_tiles_to_place == 0 {
            return !self.accept(target_continous_damaged);
        }
        return false;
    }

    fn accept(&self, target_continous_damaged: &Vec<u8>) -> bool {
        return count_continous_damaged(&self.tiles) == *target_continous_damaged;
    }
}

struct ChildSolutions {
    solution: SolutionCandidate,
    index: usize,
    previous_state: Tile,
}

impl ChildSolutions {
    fn from_candidate(s: &SolutionCandidate) -> Self {
        return Self {
            solution: s.clone(),
            index: s.tiles.iter().position(|t| {
                if let Tile::Unknown = t {
                    return true;
                } else {
                    return false;
                }
            }).unwrap(),
            previous_state: Tile::Unknown,
        }
    }
}

impl Iterator for ChildSolutions {
    type Item = SolutionCandidate;

    fn next(&mut self) -> Option<Self::Item> {
        let new_tile = match self.previous_state {
            Tile::Unknown => Some(Tile::Damaged),
            Tile::Damaged => Some(Tile::Operational),
            Tile::Operational => None
        };
        return new_tile.map(|t| {
            self.previous_state = t;
            let mut new_tiles = self.solution.tiles.clone();
            new_tiles[self.index] = t;
            return SolutionCandidate {
                tiles: new_tiles,
                unknown_tiles: self.solution.unknown_tiles - 1,
                damaged_tiles_to_place: if let Tile::Damaged = t {
                    self.solution.damaged_tiles_to_place.saturating_sub(1)
                } else {
                    self.solution.damaged_tiles_to_place
                }
            }
        })
    }
}


fn parse_puzzle(input: &str) -> IResult<&str, Puzzle> {
    let (remaining, val) = separated_pair(
        parse_tiles,
        space1,
        separated_list1(tag(","), nom::character::complete::u8)
    )(input)?;
    return Ok((
        remaining,
        Puzzle {
            tiles: val.0,
            continuous_damaged: val.1
        }
    ))
}

#[derive(Clone, Copy, PartialEq, Eq, Debug)]
enum Tile {
    Operational,
    Damaged,
    Unknown
}

fn parse_tiles(input: &str) -> IResult<&str, Vec<Tile>> {
    return many1(parse_tile)(input);
}

fn parse_tile(input: &str) -> IResult<&str, Tile> {
    return alt((
        value(Tile::Operational, tag(".")),
        value(Tile::Damaged, tag("#")),
        value(Tile::Unknown, tag("?")),
    ))(input);
}

#[cfg(test)]
mod tests {
    use crate::{parse_puzzle, Puzzle, Tile, SolutionCandidate, ChildSolutions, count_continous_damaged, parse_tiles};

    #[test]
    fn puzzles_are_parsed_correctly() {
        let input = "?#???#.???.??????. 2,1,1,1,1,4";
        let (_, parsed_puzzle) = parse_puzzle(input).unwrap();
        assert_eq!(
            parsed_puzzle,
            Puzzle {
                tiles: vec![
                    Tile::Unknown,
                    Tile::Damaged,
                    Tile::Unknown,
                    Tile::Unknown,
                    Tile::Unknown,
                    Tile::Damaged,
                    Tile::Operational,
                    Tile::Unknown,
                    Tile::Unknown,
                    Tile::Unknown,
                    Tile::Operational,
                    Tile::Unknown,
                    Tile::Unknown,
                    Tile::Unknown,
                    Tile::Unknown,
                    Tile::Unknown,
                    Tile::Unknown,
                    Tile::Operational
                ],
                continuous_damaged: vec![2, 1, 1, 1, 1, 4]
            }
        )
    }

    fn solution_candidate(input: &str) -> SolutionCandidate {
        let (_, puzzle) = parse_puzzle(input).unwrap();
        return SolutionCandidate::from_puzzle(&puzzle);
    }

    #[test]
    fn simple_solution_candidates_is_created_correctly() {
        let candidate = solution_candidate("???.### 1,1,3");
        assert_eq!(candidate.unknown_tiles, 3);
        assert_eq!(candidate.damaged_tiles_to_place, 2);
    }

    #[test]
    fn longer_solution_candidates_is_created_correctly() {
        let candidate = solution_candidate("?#?.?#?..#???.#.???# 2,2,3,1,1,1");
        assert_eq!(candidate.unknown_tiles, 10);
        assert_eq!(candidate.damaged_tiles_to_place, 5);
    }

    #[test]
    fn child_solution_are_computed_correctly() {
        let candidate = solution_candidate("???.### 1,1,3");
        let children: Vec<SolutionCandidate> = ChildSolutions::from_candidate(&candidate).collect();
        assert_eq!(
            children,
            vec![
                solution_candidate("#??.### 1,1,3"),
                solution_candidate(".??.### 1,1,3"),
            ]
        )
    }

    #[test]
    fn continous_damaged_are_counted_correctly() {
        let tiles = parse_tiles("#.#.###").unwrap().1;
        let counts = count_continous_damaged(&tiles);
        assert_eq!(counts, vec![1, 1, 3])
    }

    #[test]
    fn a_valid_solution_is_accepted() {
        let input = "#.#.### 1,1,3";
        let puzzle = parse_puzzle(input).unwrap().1;
        let solution = SolutionCandidate::from_puzzle(&puzzle);
        assert!(solution.accept(&puzzle.continuous_damaged))
    }

    #[test]
    fn an_incomplete_valid_solution_is_accepted() {
        let input = "#?#?### 1,1,3";
        let puzzle = parse_puzzle(input).unwrap().1;
        let solution = SolutionCandidate::from_puzzle(&puzzle);
        assert!(solution.accept(&puzzle.continuous_damaged));
    }
}