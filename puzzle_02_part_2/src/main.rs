use std::env;
use std::fs::File;
use std::io::{BufReader, BufRead};
use std::process::exit;

use nom::IResult;
use nom::branch::alt;
use nom::bytes::complete::tag;
use nom::character::complete::{space1, space0};
use nom::combinator::value;
use nom::multi::separated_list1;
use nom::sequence::{delimited, separated_pair, tuple};

fn main() {
    let args: Vec<String> = env::args().collect();
    if args.len() != 2 {
        println!("usage : {} FILE", &args[0]);
        exit(1);
    }
    let f = File::open(&args[1]).unwrap();
    let reader = BufReader::new(f);
    let total: u32 = reader
        .lines()
        .map(|l| parse_game(l.unwrap().as_str()).unwrap().1)
        .map(|g| g.power())
        .sum();
    println!("Total : {}", total);
}

struct Game {
    _id: u32,
    draws: Vec<Draw>
}

impl Game {
    fn power(&self) -> u32 {
        let Draw{red, green, blue} = self.minimum_set();
        return red * green * blue;
    }

    fn minimum_set(&self) -> Draw {
        Draw {
            red: self.draws.iter().map(|d| d.red).max().unwrap(),
            green: self.draws.iter().map(|d| d.green).max().unwrap(),
            blue: self.draws.iter().map(|d| d.blue).max().unwrap(),
        }
    }
}

fn parse_game(input: &str) -> IResult<&str, Game> {
    let (remainder, (id, draws)) = tuple((
        parse_game_id,
        parse_draws
    ))(input).unwrap();
    return Ok((remainder, Game{_id: id, draws}));
}

fn parse_game_id(input: &str) -> IResult<&str, u32> {
    return delimited(tag("Game "), nom::character::complete::u32, tag(":"))(input);
}

fn parse_draws(input: &str) -> IResult<&str, Vec<Draw>> {
    return separated_list1(tag(";"), parse_draw)(input);
}

struct Draw {
    red: u32,
    green: u32,
    blue: u32,
}

fn parse_draw(input: &str) -> IResult<&str, Draw> {
    let (remainder, vec) = separated_list1(
        tag(","),
        delimited(
            space0, 
            parse_color_count, 
            space0
        )
    )(input).unwrap();
    let mut draw = Draw{red: 0, green: 0, blue: 0};
    for cc in vec {
        match cc.color {
            Color::Red => {draw.red += cc.count}
            Color::Green => {draw.green += cc.count}
            Color::Blue => {draw.blue += cc.count}
        }
    }
    return Ok((remainder, draw));
}

struct ColorCount {
    color: Color,
    count: u32,
}

fn parse_color_count(input: &str) -> IResult<&str, ColorCount> {
    let (remainder, (count, color)) = separated_pair(
        nom::character::complete::u32,
        space1,
        parse_color
    )(input)?;

    return Ok((remainder, ColorCount{color, count}));
}

#[derive(Clone, Copy)]
enum Color {
    Red,
    Green,
    Blue,
}

fn parse_color(input: &str) -> IResult<&str, Color> {
    return alt((
        value(Color::Red, tag("red")),
        value(Color::Green, tag("green")),
        value(Color::Blue, tag("blue")),
    ))(input);
}
