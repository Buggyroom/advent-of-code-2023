
use std::{fs::File, io::{BufReader, BufRead}, collections::{HashSet, HashMap}, ops::{Add, Deref}};

use nom::{IResult, sequence::{tuple, separated_pair, delimited}, bytes::complete::tag, character::complete::{space1, space0}, multi::separated_list1, combinator::all_consuming};

fn main() {
    let args: Vec<String> = std::env::args().collect();
    if args.len() != 2 {
        println!("usage : {} FILE", &args[0]);
        std::process::exit(1);
    }
    let f = File::open(&args[1]).unwrap();
    let reader = BufReader::new(f);
    let mut cards_per_id: HashMap<u32, u32> = HashMap::new();
    for (i, line) in reader.lines().enumerate() {
        let line = line.unwrap();
        let (_, card) = parse_card(&line).unwrap();
        *cards_per_id.entry(card.id).or_insert(0) += 1;
        let multiplicity = *cards_per_id.get(&card.id).unwrap();
        for i in card.id+1..card.id+1+card.matching_numbers() {
            *cards_per_id.entry(i).or_insert(0) += multiplicity;
        }
    }
    let total: u32 = (1..=209).map(|i| cards_per_id.get(&i).unwrap()).sum();
    println!("Total : {}", total);
}

struct Card {
    id: u32,
    winning: Vec<u32>,
    numbers: Vec<u32>,
}

impl Card {
    fn points(&self) -> u32 {
        let count = self.matching_numbers();
        if count == 0 {
            return 0;
        } else {
            return 2u32.pow((count as u32) - 1);
        }
    }

    fn matching_numbers(&self) -> u32 {
        let winning: HashSet<u32> = self.winning.iter().cloned().collect();
        return self.numbers.iter().filter(|n| winning.contains(n)).count() as u32;
    }
}

fn parse_card(input: &str) -> IResult<&str, Card> {
    let (remainder, val) = all_consuming(tuple((
        parse_card_number,
        tag(":"),
        separated_pair(
            delimited(space0, parse_number_list, space0),
            tag("|"),
            delimited(space0, parse_number_list, space0),
        ),
    )))(input)?;

    return Ok((remainder, Card {
        id: val.0,
        winning: val.2.0,
        numbers: val.2.1,
    }));
}

fn parse_number_list(input: &str) -> IResult<&str, Vec<u32>> {
    return separated_list1(space1, nom::character::complete::u32)(input);
}

fn parse_card_number(input: &str) -> IResult<&str, u32> {
    let (remainder, val) = tuple((
        tag("Card"),
        space1,
        nom::character::complete::u32
    ))(input)?;

    return Ok((remainder, val.2));
}