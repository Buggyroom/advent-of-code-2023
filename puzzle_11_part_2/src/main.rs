use std::{fs::File, io::{BufReader, BufRead}, collections::HashSet};

fn main() {
    let args: Vec<String> = std::env::args().collect();
    if args.len() != 2 {
        println!("usage : {} FILE", &args[0]);
        std::process::exit(1);
    }
    let f = File::open(&args[1]).unwrap();
    let reader = BufReader::new(f);
    let mut stars: Vec<Position> = Vec::new();
    let mut rows_with_stars: HashSet<u64> = HashSet::new();
    let mut columns_with_stars: HashSet<u64> = HashSet::new();
    for (y, line) in reader.lines().enumerate() {
        let line = line.unwrap();
        line
            .chars()
            .enumerate()
            .filter(|(_, c)| *c == '#')
            .for_each(|(x, _)| {
                stars.push(Position { x: x as u64, y: y as u64});
                columns_with_stars.insert(x as u64);
                rows_with_stars.insert(y as u64);
            });
    }
    let height = stars.iter().map(|s| s.y).max().unwrap() + 1;
    let extra_y_space = cumulative_sum(&rows_with_stars, height);
    let width = stars.iter().map(|s| s.x).max().unwrap() + 1;
    let extra_x_space = cumulative_sum(&columns_with_stars, width);
    stars.iter_mut().for_each(|s| {
        s.x += extra_x_space[s.x as usize];
        s.y += extra_y_space[s.y as usize];
    });
    let mut result: u64 = 0;
    for i in 0..stars.len() {
        for j in 0..i {
            if i != j {
                result += stars[i].l1_distance(&stars[j]);
            }
        }
    }
    println!("Result : {}", result);
}

fn cumulative_sum(stars: &HashSet<u64>, size: u64) -> Vec<u64> {
    let mut res: Vec<u64> = Vec::new();
    for i in 0..size {
        let previous = res.last().unwrap_or(&0).to_owned();
        let no_stars = !stars.contains(&i);
        if no_stars {
            res.push(previous + 1000000 - 1);
        } else {
            res.push(previous);
        }
    }
    return res;
}

struct Position {
    x: u64,
    y: u64
}

impl Position {
    fn l1_distance(&self, other: &Self) -> u64 {
        return self.x.abs_diff(other.x) + self.y.abs_diff(other.y);
    }
}
