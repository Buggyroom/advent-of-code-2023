use std::{
    fs::File,
    io::{BufReader, Read},
};

use nom::{
    bytes::complete::tag,
    character::complete::{alpha1, multispace0, space1},
    multi::separated_list1,
    sequence::{delimited, preceded, separated_pair, terminated},
    IResult,
};

fn main() {
    let args: Vec<String> = std::env::args().collect();
    if args.len() != 2 {
        println!("usage : {} FILE", &args[0]);
        std::process::exit(1);
    }
    let f = File::open(&args[1]).unwrap();
    let mut reader = BufReader::new(f);
    let mut buffer = String::new();
    let _ = reader.read_to_string(&mut buffer).unwrap();
    let (_, almanac) = parse_almanac(buffer.as_str()).unwrap();
    let result = almanac
        .seeds
        .iter()
        .map(|s| almanac.transform_number(*s))
        .min()
        .unwrap();
    println!("Result : {}", result);
}

struct Almanac {
    seeds: Vec<i64>,
    maps: Vec<Map>,
}

impl Almanac {
    fn transform_number(&self, input: i64) -> i64 {
        let mut result = input;
        for map in &self.maps {
            result = map.transform_number(result);
        }
        return result;
    }
}

fn parse_almanac(input: &str) -> IResult<&str, Almanac> {
    let (remainder, val) = delimited(
        multispace0,
        separated_pair(
            parse_seeds,
            multispace0,
            separated_list1(multispace0, parse_map),
        ),
        multispace0,
    )(input)?;

    return Ok((
        remainder,
        Almanac {
            seeds: val.0,
            maps: val.1,
        },
    ));
}

fn parse_seeds(input: &str) -> IResult<&str, Vec<i64>> {
    return preceded(
        tag("seeds: "),
        separated_list1(space1, nom::character::complete::i64),
    )(input);
}

struct Map {
    ranges: Vec<MapLine>,
}

impl Map {
    fn transform_number(&self, input: i64) -> i64 {
        for range in &self.ranges {
            if input >= range.source_start && input < range.source_start + range.length {
                return range.destination_start + input - range.source_start;
            }
        }
        return input;
    }
}

fn parse_map(input: &str) -> IResult<&str, Map> {
    let (remainder, (_, ranges)) = separated_pair(
        parse_map_kind,
        multispace0,
        separated_list1(multispace0, parse_map_line),
    )(input)?;
    return Ok((remainder, Map { ranges }));
}

fn parse_map_kind(input: &str) -> IResult<&str, (&str, &str)> {
    return terminated(separated_pair(alpha1, tag("-to-"), alpha1), tag(" map:"))(input);
}

struct MapLine {
    source_start: i64,
    destination_start: i64,
    length: i64,
}

fn parse_map_line(input: &str) -> IResult<&str, MapLine> {
    let (remainder, val) = separated_list1(space1, nom::character::complete::i64)(input)?;

    if val.len() != 3 {
        panic!("expected 3 numbers, found {}", val.len());
    }
    return Ok((
        remainder,
        MapLine {
            source_start: val[1],
            destination_start: val[0],
            length: val[2],
        },
    ));
}
