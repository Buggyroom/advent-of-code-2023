import argparse
import re
from typing import *

NUMBER = re.compile(r"\d+")

def iter_numbers(lines: Iterable[str]) -> Iterable[int]:
    for line in lines:
        for num in NUMBER.findall(line):
            yield int(num)

parser = argparse.ArgumentParser()
parser.add_argument("file", type=argparse.FileType("r"))
args = parser.parse_args()

with args.file as f:
    biggest = max(iter_numbers(f))

print(f"Biggest : {biggest}")


