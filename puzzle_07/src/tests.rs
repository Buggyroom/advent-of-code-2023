use super::*;

#[test]
fn five_of_a_kind_is_recognized() {
    let h = Hand {
        cards: [Card::Ace; 5],
        bid: 0
    };
    assert_eq!(h._type(), HandType::FiveOfAKind);
}

#[test]
fn four_of_a_kind_is_recognized() {
    let h = Hand {
        cards: [Card::Ace, Card::Ace, Card::Ace, Card::Ace, Card::Two],
        bid: 0
    };
    assert_eq!(h._type(), HandType::FourOfAKind);
}

#[test]
fn full_house_is_recognized() {
    let h = Hand {
        cards: [Card::Ace, Card::Ace, Card::Ace, Card::Two, Card::Two],
        bid: 0
    };
    assert_eq!(h._type(), HandType::FullHouse);
}

#[test]
fn three_of_a_kind_is_recognized() {
    let h = Hand {
        cards: [Card::Ace, Card::Ace, Card::Ace, Card::Two, Card::Three],
        bid: 0
    };
    assert_eq!(h._type(), HandType::ThreeOfAKind);
}

#[test]
fn two_pairs_are_recognized() {
    let h = Hand {
        cards: [Card::Ace, Card::Ace, Card::Two, Card::Two, Card::Three],
        bid: 0
    };
    assert_eq!(h._type(), HandType::TwoPair);
}

#[test]
fn one_pair_is_recognized() {
    let h = Hand {
        cards: [Card::Ace, Card::Ace, Card::Two, Card::Three, Card::Four],
        bid: 0
    };
    assert_eq!(h._type(), HandType::OnePair);
}

#[test]
fn high_card_is_recognized() {
    let h = Hand {
        cards: [Card::Two, Card::Three, Card::Four, Card::Five, Card::Six],
        bid: 0
    };
    assert_eq!(h._type(), HandType::HighCard);
}

#[test]
fn hands_are_sorted_on_type_first() {
    let a = Hand {
        cards: [Card::Ace; 5],
        bid: 0
    };

    let b = Hand {
        cards: [Card::Ace, Card::Ace, Card::Ace, Card::Ace, Card::Two],
        bid: 0
    };

    assert!(a.cmp_key() > b.cmp_key())
}

#[test]
fn hands_are_sorted_by_lexicographic_cards_order_second() {
    let a = Hand {
        cards: [Card::Three, Card::Two, Card::Four, Card::Five, Card::Six],
        bid: 0
    };

    let b = Hand {
        cards: [Card::Two, Card::Three, Card::Four, Card::Five, Card::Six],
        bid: 0
    };

    assert!(a.cmp_key() > b.cmp_key())
}

