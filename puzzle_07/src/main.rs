use std::{
    fs::File,
    io::{BufRead, BufReader}, collections::HashMap,
};

use nom::{
    branch::alt,
    bytes::complete::tag,
    character::complete::space1,
    combinator::value,
    multi::count,
    sequence::separated_pair,
    IResult,
};

fn main() {
    let args: Vec<String> = std::env::args().collect();
    if args.len() != 2 {
        println!("usage: {} FILE", &args[0]);
        std::process::exit(1);
    }
    let f = File::open(&args[1]).unwrap();
    let reader = BufReader::new(f);
    let mut hands: Vec<Hand> = reader
        .lines()
        .map(|l| parse_hand(l.unwrap().as_str()).unwrap().1)
        .collect();
    hands.sort_by_cached_key(|h| h.cmp_key());
    let winnings: u64 = hands.iter().enumerate().map(|(i, h)| (i + 1) as u64 * h.bid).sum();
    println!("Winnings : {}", winnings);
}

struct Hand {
    cards: [Card; 5],
    bid: u64,
}

impl Hand {
    fn cmp_key(&self) -> (HandType, [Card; 5]) {
        return (self._type(), self.cards)
    }

    fn _type(&self) -> HandType {
        let mut counts: HashMap<Card, u8> = HashMap::new();
        self.cards.iter().for_each(|c| *counts.entry(*c).or_insert(0) += 1);
        let mut counts: Vec<u8> = counts.into_values().collect();
        counts.sort();
        counts.reverse();
        match &counts[..] {
            [5] => HandType::FiveOfAKind,
            [4, 1] => HandType::FourOfAKind,
            [3, 2] => HandType::FullHouse,
            [3, 1, 1] => HandType::ThreeOfAKind,
            [2, 2, 1] => HandType::TwoPair,
            [2, 1, 1, 1] => HandType::OnePair,
            _ => HandType::HighCard,
        }
    }
}

#[derive(Debug, Clone, Copy, PartialEq, Eq, PartialOrd, Ord)]
enum HandType {
    HighCard,
    OnePair,
    TwoPair,
    ThreeOfAKind,
    FullHouse,
    FourOfAKind,
    FiveOfAKind
}

fn parse_hand(input: &str) -> IResult<&str, Hand> {
    let (remainder, (card_vec, bid)) =
        separated_pair(count(parse_card, 5), space1, nom::character::complete::u64)(input)?;

    return Ok((
        remainder,
        Hand {
            cards: [
                card_vec[0],
                card_vec[1],
                card_vec[2],
                card_vec[3],
                card_vec[4],
            ],
            bid,
        },
    ));
}

#[derive(Debug, Clone, Copy, PartialEq, Eq, PartialOrd, Ord, Hash)]
enum Card {
    Two,
    Three,
    Four,
    Five,
    Six,
    Seven,
    Eight,
    Nine,
    Ten,
    Jack,
    Queen,
    King,
    Ace,
}

fn parse_card(input: &str) -> IResult<&str, Card> {
    return alt((
        value(Card::Two, tag("2")),
        value(Card::Three, tag("3")),
        value(Card::Four, tag("4")),
        value(Card::Five, tag("5")),
        value(Card::Six, tag("6")),
        value(Card::Seven, tag("7")),
        value(Card::Eight, tag("8")),
        value(Card::Nine, tag("9")),
        value(Card::Ten, tag("T")),
        value(Card::Jack, tag("J")),
        value(Card::Queen, tag("Q")),
        value(Card::King, tag("K")),
        value(Card::Ace, tag("A")),
    ))(input);
}


#[cfg(test)]
mod tests;
